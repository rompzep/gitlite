var json;
var idRepo = 0;
var defaultBranch = "master";
var token="WGoyAoQbmRWAzeEXD-xa";

$(document).ready(function() { 

	$.ajax({
		type: 'GET',
		url: "https://gitlab.com/api/v4/projects",
		data: {
			private_token: token,
			owned: true,
		},
		mimeType: 'json',

		success: function(data) {
			json=data;
			t=$("#dataTable").DataTable();
			$.each(data, function(i, repo) {
			dateActivity=new Date(repo.last_activity_at);
			dateCreated=new Date(repo.created_at);

					t.row.add([repo.name, 
										 repo.namespace.name, 
										 dateCreated.toLocaleString(),
										 dateActivity.toLocaleString(),
										 repo.visibility, 
										 repo.archived ? "Archivé" : "Actif",
										 repo.star_count 

										]).draw();
			});
		},
		error: function(xhr) {
			alert('API des repos indisponible');
		}
	});
	

	$('#dataTable tbody').on('click', 'tr', function () {
		let table = $('#dataTable').DataTable();
		let data = table.row(this).data();
		replaceByRepo(getID(json, data[0]), "master");
		openTabs();
	})
})
 
function openTabs() {
	var prefix = '<li class="nav-item"><a class="nav-link" href="#" onclick="';
	var suffix = '</span></a></li>';
	var pages = ['commits();\"', 'stats();\"'];
	var titres = ["Commits", "Statistiques"];
	for (i=0; i<titres.length; i++) {
		$(prefix + pages[i] + '"><span>' + titres[i] + suffix).insertAfter($('#onglets'));
	}
}

function stats() {
	$(".container-fluid").detach();
	$("#custom").load("custom_html/stats.html", function() {
		replaceByStats("master");
	});
}

function commits() {
	$(".container-fluid").detach();
	$("#custom").load("custom_html/commits.html", function() {
		replaceByCommits("master");
	});
}
 
// Récupère l'ID du commit _nom_ dans le fichier JSon _data_
function getID(data, nom) {
	for(let x in data){
		if(data[x].name.localeCompare(nom)==0) {
			idRepo=data[x].id;
			return data[x].id;
		}
	}
}

function getPath(data,nom) {
	for(let x in data){
		if(data[x].name.localeCompare(nom)==0)
			return data[x].path;
	}
} 


/** Affiche dans le div principal une table décrivant le contenu d'un dossier
* id : identifiant du repo
* branchName : nom de la branche
* path : path du dossier, "" (root) par défaut
*/
function replaceByRepo(id, branchName, path="") {
	let activeBranch={name : branchName};
	console.log("replaceByRepo, "+activeBranch.name);
	$(".container-fluid").detach();
	$("#custom").load("custom_html/repo.html", function() {
		getListBranches(id, activeBranch);
		getRepoData(id, activeBranch, path);
	});
}

function getRepoData(id, activeBranch, path) {
	let json;
	console.log(path)
	$.ajax({
		type: 'GET',
		url: "https://gitlab.com/api/v4/projects/"+id+"/repository/tree",
		data: {
			private_token: token,
			path: path,
			ref: activeBranch.name
		},
		mimeType: 'json',

		success: function(data) {
			json=data;

			console.log(json);
			
			t=$("#tableRepo").DataTable();
			$.each(data, function(i,file) {		

					t.row.add([file.name, 
							 file.type=="blob" ? "Fichier" : "Répertoire"
								]).draw();
			});
		},
		error: function(xhr) {
			alert('API du repo indisponible');
		} 			
	});

	$("#tableRepo tbody").on('click', 'tr', function () {
			let table = $('#tableRepo').DataTable();
			let data = table.row(this).data();
			if(data[1].localeCompare("Fichier")==0)
				openEditor(id, getPath(json, data[0]), activeBranch);
			else if(data[1].localeCompare("Répertoire")==0) {
				replaceByRepo(id, activeBranch.name, path+data[0]+"/");
			}
	});
}


function getListBranches(projectID, activeBranch) {

	$.ajax({
		type: 'GET',
		url: "https://gitlab.com/api/v4/projects/"+projectID+"/repository/branches", 
		data: {
			private_token: token,
		},
		mimeType: 'json',	

		success: function(data) {

			console.log(data);
			$.each(data, function(i, branch) {
				let appended=$("<h6 class=\"collapse-head\">"+branch.name+"</h6>");
				$("#list_branchs").append(appended);

				appended.click(function() {
					activeBranch.name=this.innerHTML;
					replaceByRepo(projectID, activeBranch.name);
				});
	});
		},
		error: function(xhr) {
			alert("API des branches indisponible");
		}
	});	
}

function openEditor(projectID, filePath, activeBranch) {
	let fileContent;

	filePath=encodeURIComponent(filePath);

	$.ajax({
		type: 'GET',
		url: "https://gitlab.com/api/v4/projects/"+projectID+"/repository/files/"+filePath, 
		data: {
			private_token: token,
			owned: true,
			ref: activeBranch.name
		},
		mimeType: 'json',	

		success: function(file) {

			console.log(file);
			replaceByEditor(file, projectID, activeBranch);
		},
		error: function(xhr) {
			alert("Ace Editor ou API du fichier indisponible");
		}
	});
}

function replaceByEditor(file, projectID, activeBranch) {
	map=[]
	map['ahk']='autohotkey';
	map['c']='c';
	map['cs']='csharp';
	map['css']='css';
	map['html']='html';
	map['js']='javascript';
	map['md']='markdown';
	map['py']='python';
	map['php']='php';
	map['rb']='ruby';

	let content=atob(file.content);
	let extension = file.file_name.split('.').pop();
	extension=map[extension];

	$(".container-fluid").detach();
	$("#custom").load("custom_html/editor.html", function() {

	let editor;

	$.getScript("ace/ace.js", function() {
	    editor = ace.edit("editor");
			editor.setTheme("ace/theme/terminal");
			if(extension)
				editor.session.setMode("ace/mode/"+extension);
			editor.setValue(content);
	});

	$("#cancel").click(function() { 
		replaceByRepo(projectID, activeBranch.name)
	});

	$("#commitBtn").click(function() { 
			let branch=activeBranch.name;
			let message=$("#commitDesc").val();
			let actions=[]
console.log(projectID);
			actions['action']='update';
			actions['file_path']=file.file_path;
			actions['content']=editor.getValue();
			sendCommit(projectID, branch,message,actions);			
		});
	});
}


// dysfonctionnel, CORS header
function sendCommit(projectID,branch,message,actions) {

	$.ajax({
		type: 'POST',
		url: "https://cors-anywhere.herokuapp.com/https://gitlab.com/api/v4/projects/"+projectID+"/repository/commits", 
		headers: {
			"Content-Type":'application/json',
			"Private-Token":token,
		},

		data: {
			branch: branch,
			commit_message: message,
			actions: [{
				action: actions['action'],
				file_path: actions['file_path'],
				content: actions['content']
			}]
		},

		success: function(file) {
			console.log(file);
		},
		error: function(xhr) {
			console.log(xhr);
		}
	});
}

