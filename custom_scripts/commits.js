console.log("----< commits.js >----");

$.ajax({
    type: 'GET',
    url: "https://gitlab.com/api/v4/projects/"+idRepo+"/repository/branches", 
    data: {
      private_token: token
    },
    mimeType: 'json', 

    success: function(data) {

      console.log(data);
      $.each(data, function(i, branch) {
        let appended=$("<h6 class=\"collapse-head\">"+branch.name+"</h6>");
        $("#list_branchs_commits").append(appended);

        appended.click(function() {
          branche = this.innerHTML;
          console.log(branche);
          $(".container-fluid").detach();
          $("#custom").load("custom_html/commits.html", function() {
            replaceByCommits(branche);
          });
        });
  });
    },
    error: function(xhr) {
      alert("API indisponible");
    }
  });

function replaceByCommits(branche) {
  $("#nom-branche-commits").append(" : Branche "+branche);

  $.ajax({
  type: 'GET',
  url: "https://gitlab.com/api/v4/projects/"+idRepo+"/repository/commits",
  data: {
    private_token: token,
    owned: true,
    with_stats: true,
    ref_name: branche
  },
  mimeType: 'json',

  success: function(data) {
    console.log(data);
    table = $("#dataTable").DataTable();
    $.each(data, function(i, commit) {
      date = new Date(commit.created_at);
      table.row.add([
        commit.author_name,
        commit.title,
        date.toLocaleString()
      ]).draw();
    });
  },

  error: function(xhr) {
    alert('rip');
  } 
});
}
