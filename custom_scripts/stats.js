function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

$.ajax({
		type: 'GET',
		url: "https://gitlab.com/api/v4/projects/"+idRepo+"/repository/branches", 
		data: {
			private_token: token
		},
		mimeType: 'json',	

		success: function(data) {

			console.log(data);

			$.each(data, function(i, branch) {
				let appended=$("<h6 class=\"collapse-head\">"+branch.name+"</h6>");
				$("#list_branchs").append(appended);

				appended.click(function() {
					activeBranch = this.innerHTML;
					console.log(activeBranch);
					$(".container-fluid").detach();
					$("#custom").load("custom_html/stats.html", function() {
						replaceByStats(activeBranch);
					});
				});
			});
		},

		error: function(xhr) {
			console.log(xhr);
			alert("API indisponible");
		}
	});	


function replaceByStats(activeBranch) {
	$("#nom-branche-stats").append(" : Branche "+activeBranch);

	$.ajax({
	type: 'GET',
	url: "https://gitlab.com/api/v4/projects/"+idRepo+"/repository/branches",
	data: {
		private_token: token,
		owned: true
	},
	mimeType: 'json',

	success: function(data) {
		console.log("nb branches : "+data.length);
		$("#nb-branches").append(data.length);
	},

	error: function(xhr) {
		alert('rip');
	}	
});

$.ajax({
	type: 'GET',
	url: "https://gitlab.com/api/v4/projects/"+idRepo+"/members",
	data: {
		private_token: token,
		owned: true,
	},
	mimeType: 'json',

	success: function(data) {
		console.log("nb membres : "+data.length);
		$("#nb-membres").append(data.length);
	},
	
	error: function(xhr) {
		alert('rip');
	}	
});

$.ajax({
	type: 'GET',
	url: "https://gitlab.com/api/v4/projects/"+idRepo+"/repository/commits",
	data: {
		private_token: token,
		owned: true,
		with_stats: true,
		ref_name: activeBranch
	},
	mimeType: 'json',

	success: function(data) {
		console.log("commits : "+activeBranch);
		console.log("nb commits : "+data.length);
		console.log(data);
		$("#nb-commits").append(data.length);
	
		// Set new default font family and font color to mimic Bootstrap's default styling
		Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
		Chart.defaults.global.defaultFontColor = '#858796';

		authors = []
		nbCommits = []
		additions = []
		deletions = []

		$.each(data, function(i, commit) {
	      if (!authors.includes(commit.author_name)) {
	      	authors.push(commit.author_name);
	      	additions.push(commit.stats.additions);
	      	deletions.push(commit.stats.deletions);
	      	nbCommits.push(1);
	      }
	      else {
	      	nbCommits[authors.indexOf(commit.author_name)]++;
	      	additions[authors.indexOf(commit.author_name)] += commit.stats.additions;
	      	deletions[authors.indexOf(commit.author_name)] += commit.stats.deletions;
	      }
	  	});
		console.log(authors);
		console.log(additions);
		console.log(deletions);

		nbAdditions = additions.reduce((a,b) => a+b, 0);
		nbDeletions = deletions.reduce((a,b) => a+b, 0);
		$("#ratio-add-del").append( (nbAdditions / nbDeletions).toFixed(2) );
		$("#nb-additions").append(nbAdditions+" Ajouts de Lignes");
		$("#nb-deletions").append(nbDeletions+" Suppressions de Lignes");
	

		// Contributions
		var ctx = document.getElementById("contributions");
		var myBarChart = new Chart(ctx, {
		  type: 'bar',
		  data: {
		    labels: authors,
		    datasets: [{
		      label: "Commits",
		      backgroundColor: "#4e73df",
		      hoverBackgroundColor: "#2e59d9",
		      borderColor: "#4e73df",
		      data: nbCommits,
		    }],
		  },
		  options: {
		    maintainAspectRatio: false,
		    layout: {
		      padding: {
		        left: 10,
		        right: 25,
		        top: 25,
		        bottom: 0
		      }
		    },
		    scales: {
		      xAxes: [{
		        time: {
		          unit: 'month'
		        },
		        gridLines: {
		          display: false,
		          drawBorder: false
		        },
		        ticks: {
		          maxTicksLimit: 6
		        },
		        maxBarThickness: 25,
		      }],
		      yAxes: [{
		        ticks: {
		          min: 0,
		          max: Math.max(...nbCommits),
		          maxTicksLimit: 5,
		          padding: 10,
		          // Include a dollar sign in the ticks
		          callback: function(value, index, values) {
		            return number_format(value);
		          }
		        },
		        gridLines: {
		          color: "rgb(234, 236, 244)",
		          zeroLineColor: "rgb(234, 236, 244)",
		          drawBorder: false,
		          borderDash: [2],
		          zeroLineBorderDash: [2]
		        }
		      }],
		    },
		    legend: {
		      display: false
		    },
		    tooltips: {
		      titleMarginBottom: 10,
		      titleFontColor: '#6e707e',
		      titleFontSize: 14,
		      backgroundColor: "rgb(255,255,255)",
		      bodyFontColor: "#858796",
		      borderColor: '#dddfeb',
		      borderWidth: 1,
		      xPadding: 15,
		      yPadding: 15,
		      displayColors: false,
		      caretPadding: 10,
		      callbacks: {
		        label: function(tooltipItem, chart) {
		          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
		          return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
		        }
		      }
		    },
		  }
		});

		// Langages
		var languages = []
		var portions = []
		var colors = []
		var letters = "0123456789abcdef";

		$.ajax({
		  type: 'GET',
		  url: "https://gitlab.com/api/v4/projects/"+idRepo+"/languages",
		  data: {
		    private_token: token,
		    owned: true,
		    ref_name: activeBranch
		  },
		  mimeType: 'json',

		  success: function(data) {
		    for (var key in data) {
		      languages.push(key)
		      portions.push(data[key])
		      color = '#'; 
		      for (var i = 0; i < 6; i++) 
		        color += letters[(Math.floor(Math.random() * 16))];
		      colors.push(color);
		    }

		    $("#nb-langages").append(languages.length+" Langages");

		    var ctx = document.getElementById("langages");
		    var myPieChart = new Chart(ctx, {
		      type: 'doughnut',
		      data: {
		        labels: languages,
		        datasets: [{
		          data: portions,
		          backgroundColor: colors
		        }],
		      },
		      options: {
		        maintainAspectRatio: false,
		        tooltips: {
		          backgroundColor: "rgb(255,255,255)",
		          bodyFontColor: "#858796",
		          borderColor: '#dddfeb',
		          borderWidth: 1,
		          xPadding: 15,
		          yPadding: 15,
		          displayColors: false,
		          caretPadding: 10,
		        },
		        legend: {
		          display: false
		        },
		        cutoutPercentage: 80,
		      },
		    });
		  },
		  
		  error: function(xhr) {
		    alert('rip');
		  } 
		});

		// Ajouts Lignes
		var ctx = document.getElementById("additions");
		var myBarChart = new Chart(ctx, {
		  type: 'bar',
		  data: {
		    labels: authors,
		    datasets: [{
		      label: "Lignes",
		      backgroundColor: "#00fe7e",
		      hoverBackgroundColor: "#01d758",
		      borderColor: "#4e73df",
		      data: additions,
		    }],
		  },
		  options: {
		    maintainAspectRatio: false,
		    layout: {
		      padding: {
		        left: 10,
		        right: 25,
		        top: 25,
		        bottom: 0
		      }
		    },
		    scales: {
		      xAxes: [{
		        time: {
		          unit: 'month'
		        },
		        gridLines: {
		          display: false,
		          drawBorder: false
		        },
		        ticks: {
		          maxTicksLimit: 6
		        },
		        maxBarThickness: 25,
		      }],
		      yAxes: [{
		        ticks: {
		          min: 0,
		          max: Math.max(...additions),
		          maxTicksLimit: 5,
		          padding: 10,
		          // Include a dollar sign in the ticks
		          callback: function(value, index, values) {
		            return number_format(value);
		          }
		        },
		        gridLines: {
		          color: "rgb(234, 236, 244)",
		          zeroLineColor: "rgb(234, 236, 244)",
		          drawBorder: false,
		          borderDash: [2],
		          zeroLineBorderDash: [2]
		        }
		      }],
		    },
		    legend: {
		      display: false
		    },
		    tooltips: {
		      titleMarginBottom: 10,
		      titleFontColor: '#6e707e',
		      titleFontSize: 14,
		      backgroundColor: "rgb(255,255,255)",
		      bodyFontColor: "#858796",
		      borderColor: '#dddfeb',
		      borderWidth: 1,
		      xPadding: 15,
		      yPadding: 15,
		      displayColors: false,
		      caretPadding: 10,
		      callbacks: {
		        label: function(tooltipItem, chart) {
		          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
		          return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
		        }
		      }
		    },
		  }
		});

		// Suppressions Lignes
		var ctx = document.getElementById("deletions");
		var myBarChart = new Chart(ctx, {
		  type: 'bar',
		  data: {
		    labels: authors,
		    datasets: [{
		      label: "Lignes",
		      // backgroundColor: "#e74a3b",
		      backgroundColor: "#fd4626",
		      hoverBackgroundColor: "#ed0000",
		      borderColor: "#4e73df",
		      data: deletions,
		    }],
		  },
		  options: {
		    maintainAspectRatio: false,
		    layout: {
		      padding: {
		        left: 10,
		        right: 25,
		        top: 25,
		        bottom: 0
		      }
		    },
		    scales: {
		      xAxes: [{
		        time: {
		          unit: 'month'
		        },
		        gridLines: {
		          display: false,
		          drawBorder: false
		        },
		        ticks: {
		          maxTicksLimit: 6
		        },
		        maxBarThickness: 25,
		      }],
		      yAxes: [{
		        ticks: {
		          min: 0,
		          max: Math.max(...deletions),
		          maxTicksLimit: 5,
		          padding: 10,
		          // Include a dollar sign in the ticks
		          callback: function(value, index, values) {
		            return number_format(value);
		          }
		        },
		        gridLines: {
		          color: "rgb(234, 236, 244)",
		          zeroLineColor: "rgb(234, 236, 244)",
		          drawBorder: false,
		          borderDash: [2],
		          zeroLineBorderDash: [2]
		        }
		      }],
		    },
		    legend: {
		      display: false
		    },
		    tooltips: {
		      titleMarginBottom: 10,
		      titleFontColor: '#6e707e',
		      titleFontSize: 14,
		      backgroundColor: "rgb(255,255,255)",
		      bodyFontColor: "#858796",
		      borderColor: '#dddfeb',
		      borderWidth: 1,
		      xPadding: 15,
		      yPadding: 15,
		      displayColors: false,
		      caretPadding: 10,
		      callbacks: {
		        label: function(tooltipItem, chart) {
		          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
		          return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
		        }
		      }
		    },
		  }
		});
	},
	
	error: function(xhr) {
		alert('rip');
	}	
});
}